package com.gesti.n.de.ahorro.transacciones.entidades;

public class transaccion {
    private Integer id;
    private String cuenta_origen;
    private String cuenta_destino;
    private Double monto_enviar;
    private String tipo; //transferencia , deposito y pago


    public transaccion(){

    }
    public transaccion(Integer id,String cuenta_origen,String cuenta_destino,Double monto_enviar,String tipo){
        super();
        this.id=id;
        this.cuenta_origen=cuenta_origen;
        this.cuenta_destino=cuenta_destino;
        this.monto_enviar=monto_enviar;
        this.tipo=tipo;

        
    }
    
    public String getCuenta_destino() {
        return cuenta_destino;
    }
    public void setCuenta_destino(String cuenta_destino) {
        this.cuenta_destino = cuenta_destino;
    }
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getCuenta_origen() {
        return cuenta_origen;
    }
    public void setCuenta_origen(String cuenta_origen) {
        this.cuenta_origen = cuenta_origen;
    }
    public Double getmonto_enviar() {
        return monto_enviar;
    }
    public void setmonto_enviar(Double monto_enviar) {
        this.monto_enviar = monto_enviar;
    }
    public String gettipo() {
        return tipo;
    }
    public void settipo(String tipo) {
        this.tipo = tipo;
    }


    @Override
    public String toString() {
        return "transaccion [cuenta_destino=" + cuenta_destino + ", cuenta_origen=" + cuenta_origen + ", id=" + id
                + ", monto_enviar=" + monto_enviar + ", tipo=" + tipo + "]";
    } 

}
