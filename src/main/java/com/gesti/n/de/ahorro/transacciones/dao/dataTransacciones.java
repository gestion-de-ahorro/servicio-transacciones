package com.gesti.n.de.ahorro.transacciones.dao;

import com.gesti.n.de.ahorro.transacciones.entidades.logicaTransaccion;
import com.gesti.n.de.ahorro.transacciones.entidades.transaccion;
import org.springframework.stereotype.Repository;

@Repository
public class dataTransacciones {
    private static logicaTransaccion list = new logicaTransaccion();
    
    static 
    {
        list.getconsultList().add(new transaccion(1, "152522", "151522", 2005.25, "Deposito"));
        list.getconsultList().add(new transaccion(2, "585222", "585222", 1003.52, "Transferencia"));
        list.getconsultList().add(new transaccion(3, "75552", "75532",1023.52, "Pago"));
    }
    
    public logicaTransaccion getAllconsult() 
    {
        return list;
    }
    public void addconsult(transaccion consult) {
        list.getconsultList().add(consult);
    }
 
}

