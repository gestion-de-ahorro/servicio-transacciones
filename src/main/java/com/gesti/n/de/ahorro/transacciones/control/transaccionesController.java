package com.gesti.n.de.ahorro.transacciones.control;

import java.net.URI;

import com.gesti.n.de.ahorro.transacciones.dao.dataTransacciones;
import com.gesti.n.de.ahorro.transacciones.entidades.logicaTransaccion;
import com.gesti.n.de.ahorro.transacciones.entidades.transaccion;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

@RestController
@RequestMapping(path = "/transacciones")
public class transaccionesController  
{
    @Autowired
    public dataTransacciones consultDao;
    
    @GetMapping(path="/", produces = "application/json")
    public logicaTransaccion getconsult() 
    {
        return consultDao.getAllconsult();
    }
    @PostMapping(path= "/", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Object> addconsult(@RequestBody transaccion consult) 
    {
        Integer id = consultDao.getAllconsult().getconsultList().size() + 1;
        consult.setId(id);
         
        consultDao.addconsult(consult);
         
        URI location = ServletUriComponentsBuilder.fromCurrentRequest()
                                    .path("/{id}")
                                    .buildAndExpand(consult.getId())
                                    .toUri();
         
        return ResponseEntity.created(location).build();
    }

         
}
